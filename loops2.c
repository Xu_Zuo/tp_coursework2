#include <stdio.h>
#include <math.h>


#define N 729
#define reps 1000
#include <omp.h>

//#define MAX_NUM_THREADS 16

double a[N][N], b[N][N], c[N];
int jmax[N];


void init1(void);
void init2(void);
void runloop(int);
void loop1chunk(int, int);
void loop2chunk(int, int);
void valid1(void);
void valid2(void);


int main(int argc, char *argv[]) {

  double start1,start2,end1,end2;
  int r;

  init1();

  start1 = omp_get_wtime();

  for (r=0; r<reps; r++){
    runloop(1);
  }

  end1  = omp_get_wtime();

  valid1();

  printf("Total time for %d reps of loop 1 = %f\n",reps, (float)(end1-start1));


  init2();

  start2 = omp_get_wtime();

  for (r=0; r<reps; r++){
    runloop(2);
  }

  end2  = omp_get_wtime();

  valid2();

  printf("Total time for %d reps of loop 2 = %f\n",reps, (float)(end2-start2));

}

void init1(void){
  int i,j;

  for (i=0; i<N; i++){
    for (j=0; j<N; j++){
      a[i][j] = 0.0;
      b[i][j] = 3.142*(i+j);
    }
  }

}

void init2(void){
  int i,j, expr;

  for (i=0; i<N; i++){
    expr =  i%( 3*(i/30) + 1);
    if ( expr == 0) {
      jmax[i] = N;
    }
    else {
      jmax[i] = 1;
    }
    c[i] = 0.0;
  }

  for (i=0; i<N; i++){
    for (j=0; j<N; j++){
      b[i][j] = (double) (i*j+1) / (double) (N*N);
    }
  }

}


void runloop(int loopid)  {

    //int iters_rmng[MAX_NUM_THREADS];
    //int worksharing_flag[MAX_NUM_THREADS];

    int iters_rmng[omp_get_max_threads()];
    int worksharing_flag[omp_get_max_threads()];

#pragma omp parallel default(none) shared(loopid, iters_rmng, worksharing_flag)
  {
    int myid  = omp_get_thread_num();
    int nthreads = omp_get_num_threads();
    int ipt = (int) ceil((double)N/(double)nthreads);
    int lo = myid*ipt;
    int hi = (myid+1)*ipt;
    if (hi > N) hi = N;

    //printf("The iteration block on thread %d is from %d to %d\n", myid, lo, hi);

    iters_rmng[myid] = hi - lo;
    worksharing_flag[myid] = 0; //This thread's work hasn't been taken by others

    #pragma omp barrier
    //printf("Shared data stored on thread %d\n", myid);

    //Set a indicator for working threads
    //Assume threads are all busy at the beginning

    int busy_flag = 1;
    int busy_id;

    while(busy_flag == 1){

    //check one thread at a time
    #pragma omp critical
    {
      //check their own workload
      if(iters_rmng[myid] > 0){
        busy_flag = 1;
        busy_id = myid;
      }
      //If the thread has finished, it will look for the most loaded thread
      else{
        int i, max_iters = 0;

        for(i=0; i<nthreads; i++){
          if((iters_rmng[i] > max_iters) && (worksharing_flag[i] == 0)){
            busy_id = i;
            max_iters = iters_rmng[i];
          }
        }
        //printf("Thread %d detected the most loaded one is thread%d\n", myid, busy_id);

        //The checking process will be terminated if no iteration left
        if(max_iters > 0) busy_flag = 1;
        else busy_flag = 0;
      }
    }

    //If there are iterations remaining, calculate the inner chunksize
    if(busy_flag == 1){
      int chunksize, chunk_lo, chunk_hi;

    #pragma omp critical
    {
      //Update starting and ending points, since the thread might be calculating
      //chunksizes of other threads
      int high = (busy_id+1)*ipt;
      if(high > N) high = N;

      chunksize = (int) ceil((double)iters_rmng[busy_id]/(double)nthreads);
      //if(chunksize > iters_rmng[busy_id]) chunksize = iters_rmng[busy_id];

      chunk_lo = high - iters_rmng[busy_id];
      chunk_hi = chunk_lo + chunksize;

      //stealing chunk completed, update number of remaining iterations
      worksharing_flag[busy_id] = 1;
      iters_rmng[busy_id] -= chunksize;

      //if(myid != busy_id)
        //printf("thread%d stealed from thread%d\n", myid, busy_id);
    }

    //#pragma omp atomic
      //iters_rmng[busy_id] -= chunksize;

    //printf("loopid%d, myid%d, busyid%d, chunksize%d, starting point%d\n", loopid, myid, busy_id, chunksize, chunk_lo);
    //Execute chunks
    switch(loopid){
       case 1: loop1chunk(chunk_lo,chunk_hi); break;
       case 2: loop2chunk(chunk_lo,chunk_hi); break;
    }

    //#pragma omp critical
    //{
    //if(myid == 0)
      //printf("%diterations left on thread%d\n", iters_rmng[myid], myid);
    //}

    //Execution completed, set the flag back to working status
    #pragma omp critical
    {
      worksharing_flag[busy_id] = 0;
    }

    }

    }
  }
}

void loop1chunk(int lo, int hi) {
  int i,j;

  for (i=lo; i<hi; i++){
    for (j=N-1; j>i; j--){
      a[i][j] += cos(b[i][j]);
    }
  }

}



void loop2chunk(int lo, int hi) {
  int i,j,k;
  double rN2;

  rN2 = 1.0 / (double) (N*N);

  for (i=lo; i<hi; i++){
    for (j=0; j < jmax[i]; j++){
      for (k=0; k<j; k++){
	c[i] += (k+1) * log (b[i][j]) * rN2;
      }
    }
  }

}

void valid1(void) {
  int i,j;
  double suma;

  suma= 0.0;
  for (i=0; i<N; i++){
    for (j=0; j<N; j++){
      suma += a[i][j];
    }
  }
  printf("Loop 1 check: Sum of a is %lf\n", suma);

}


void valid2(void) {
  int i;
  double sumc;

  sumc= 0.0;
  for (i=0; i<N; i++){
    sumc += c[i];
  }
  printf("Loop 2 check: Sum of c is %f\n", sumc);
}
